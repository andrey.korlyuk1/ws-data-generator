#!/usr/bin/python3
import json
import random
import sys, getopt
import uuid, re
from string import Template
import requests
import exrex

url = 'https://customer-importer.stage.k8s.moneyfarm.com/external/v0/backoffice/user-import'
test_id='test'

def main(argv):
    inputfile='request.json'
    number=10
    try:
        opts, args = getopt.getopt(argv,"f:n:i:",["file=","number=", "id="])
    except getopt.GetoptError:
        print('ws-data-generator.py -f <inputfile> -n <number of items>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-f", "--file"):
            inputfile = arg
        elif opt in ("-n", "--number"):
            number = arg
        elif opt in ("-i", "--id"):
            id = arg    
    print("Input file is: ", inputfile)
    print("Customers to generate: ", number)
    with open('request.json', 'r') as file:
        template = Template(file.read())
    generateCustomers(template, int(number))
        

def generateCustomers(template, num):
    try:
        print("getting token", end='...')
        token = authorize()
    except:
        print("failure")
        sys.exit(2)
    else:
        print("success")

    i = 0
    while i < num:
        nino = generate_nino()
        ext_id = random.randrange(100000, 999999)
        email = nino+'@example.com'
        print('Customer: ', nino, ext_id, email, end='....')
        request = template.substitute(nino=nino, ext_id=ext_id, email=email, number=num, test_id=test_id)
        
        headers = {
         'Authorization': 'Bearer '+token,
         'Content-Type': 'application/json'
        }
        
        response = requests.put(url, request, headers=headers)
        if(response.status_code != 201):
            print("failure. HTTP:", response.status_code, "Body:", response.text)
        else:
            print("success")
        i += 1

def generate_nino():
    nino_regex = "(?!(?:BG|GB|KN|NK|NT|TN|ZZ))[ABCEGHJ-PRSTW-Z][ABCEGHJ-NPRSTW-Z]\\d{6}[A-D]"
    return exrex.getone(nino_regex)

def authorize():
    auth_url = "https://mf-test.eu.auth0.com/oauth/token"

    payload = json.dumps({
    "client_id": "EBFhN10VFtKGcc4Flvariy0baJVJckX2",
    "grant_type": "password",
    "username": "6999@example.com",
    "password": "password1",
    "audience": "https://mf-test.eu.auth0.com/mfa/",
    "relam": "Username-Password-Authentication",
    "scope": "openid offline_access"
    })
    headers = {
    'Content-Type': 'application/json',
    'Cookie': 'did=s%3Av0%3A445132c0-eedc-11eb-ab2d-050a6db8e0ee.2bVzcTfsdhkSEEzaMLYE8zIask3yHFYp5u0spLbqH%2Bw; did_compat=s%3Av0%3A445132c0-eedc-11eb-ab2d-050a6db8e0ee.2bVzcTfsdhkSEEzaMLYE8zIask3yHFYp5u0spLbqH%2Bw; __cf_bm=mUSlY2IOWLvCbqjXCmIH5g42l4Y292fuJB4twABrOlI-1642437827-0-AdEUB6xFc/smgAg1yUbefVMPdOuyq5vf4ZM9bu+JDpXDrbLE7bDmu0RhWIjdW9rRGkGB2MVx70sY5xY3gCwgUdk='
    }

    response = requests.request("POST", auth_url, headers=headers, data=payload)
    return json.loads(response.text)["access_token"]

if __name__ == "__main__":
   main(sys.argv[1:])