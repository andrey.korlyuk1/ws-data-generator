# ws-data-generator

generator of data for customer-importer load test

## Prerequisites

Before using the generator you should install requirements

```
pip install exrex
```

## Generating data

```
python ws-data-generator.py -n 100
```
Where `-n` - the number of customers to generate

By default it takes `request.json` as template.
It is possible to create another template, putting `{nino}`, `{ext_id}`, `{email}` placeholders and passing file path to the script

```
python ws-data-generator.py -n 100 -f path/to/file
```

## usefull queries

```
-- Get timing for test batch
select substring(c.external_id, 8, length(c.external_id)) as batch,
       substring(c.external_id, 13, length(c.external_id)) as batch_size,
       min(s.created_at) as start_time,
       max(s.finished_at) as end_time,
       TIMESTAMPDIFF(SECOND, min(s.created_at), max(s.finished_at)) as total_duration_sec,
       TIMESTAMPDIFF(SECOND, min(s.created_at), max(s.finished_at)) / substring(c.external_id, 13, length(c.external_id)) as avg_time_sec
from `saga` s
join customer c
on s.customer_id = c.id
where c.external_id like '%test%'
group by batch;
```

```
-- get customers
select id from customer
where external_id like '%test-10';
```

```
-- update settled amount
update portfolio
set settled_amount = 1000.0
where
        `type` = 'isa' and
        customer_id in (
        select id from customer
        where external_id like '%test-10');
```

```
-- update batch as failed
UPDATE customer SET external_id=CONCAT(external_id, '-fail')
where external_id like '%test-10';
```